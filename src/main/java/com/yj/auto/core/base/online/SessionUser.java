package com.yj.auto.core.base.online;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.util.ArrayUtil;
import com.yj.auto.Constants;

public class SessionUser {
	private Integer id = null;// 用户ID
	private String code = null;// 用户登录账号
	private String name = null;// 用户名称
	private Integer orgId = null;// 所属机构
	private Integer[] roles = null;// 拥有角色
	private Date time = null;// 登录时间
	private Map<String, Object> attrs = new HashMap<String, Object>();// 扩展属性

	public SessionUser(Integer id, String code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		time = new Date();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer[] getRoles() {
		return roles;
	}

	public void setRoles(Integer[] roles) {
		this.roles = roles;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Map<String, Object> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, Object> attrs) {
		this.attrs = attrs;
	}

	public Object getAttr(String key) {
		return attrs.get(key);
	}

	public void setAttr(String key, Object val) {
		this.attrs.put(key, val);
	}

	/**
	 * 判断 当前用户是否属于指定角色
	 * 
	 * @param roleId
	 *            角色ID
	 * @return
	 */
	public boolean isRoleUser(Integer roleId) {
		Integer[] arr = getRoles();
		if (null != arr) {
			return ArrayUtil.contains(arr, roleId);
		}
		return false;
	}

	/**
	 * 是否超级用户
	 * 
	 * @return
	 */
	public boolean isSuperUser() {
		return isRoleUser(Constants.ROLE_ID_SUPER_USER);
	}

	/**
	 * 是否系统管理员
	 * 
	 * @return
	 */
	public boolean isAdmin() {
		return isRoleUser(Constants.ROLE_ID_ADMIN_USER);
	}

}
