package com.yj.auto.core.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Column {

	/**
	 * 属性名
	 * 
	 * @return
	 */
	String field();

	/**
	 * 字段名
	 * 
	 * @return
	 */
	String name();

	/**
	 * 注释
	 * 
	 * @return
	 */
	String remark();
}
