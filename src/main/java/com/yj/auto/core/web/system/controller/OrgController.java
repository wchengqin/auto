package com.yj.auto.core.web.system.controller;

import java.util.*;

import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.core.web.system.model.*;
import com.yj.auto.core.web.system.service.*;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.plugin.ztree.ZTreeNode;

/**
 * 系统机构 管理
 * 
 */
@Controller(viewPath = "system")
public class OrgController extends BaseController {
	private static final Log logger = Log.getLog(OrgController.class);

	private static final String RESOURCE_URI = "org/index";
	private static final String INDEX_PAGE = "org_index.html";
	private static final String FORM_PAGE = "org_form.html";

	OrgService orgSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<Org> dt = getDataTable(query, orgSrv);
		renderJson(dt);
	}

	public void children() {
		Integer id = getParaToInt("id");
		String state = getPara("state");
		List<Org> list = orgSrv.children(id, state);
		List<ZTreeNode> result = new ArrayList<ZTreeNode>();
		for (Org root : list) {
			List<Org> subs = orgSrv.children(root.getId(), state);
			ZTreeNode t = getZTreeNode(root, subs.size() > 0);
			if (null == id && t.getIsParent()) {
				t.setOpen(true);
				for (Org obj : subs) {
					ZTreeNode st = getZTreeNode(obj, orgSrv.children(obj.getId(), state).size() > 0);
					t.addChildren(st);
				}
			}
			result.add(t);
		}
		renderJson(result);
	}

	private ZTreeNode getZTreeNode(Org c, boolean parent) {
		ZTreeNode t = new ZTreeNode(c.getId().toString(), c.getName());
		t.setNocheck(false);
		t.setIsParent(parent);
		t.addAttribute("type", c.getType());
		return t;
	}

	public boolean updateSort() {
		Integer[] id = getParaValuesToInt();
		boolean success = orgSrv.updateSort(id);
		ResponseModel<String> res = new ResponseModel<String>(success);
		renderJson(res);
		return success;
	}

	public void form() throws Exception {
		String id = getPara(0);
		Org model = null;
		if (id.startsWith("new_")) {
			model = new Org();
			model.setParentId(getParaToInt(1));
			model.setSort(getParaToInt(2));
			model.setName(getParaDecode(3));
		} else {
			model = orgSrv.get(Integer.parseInt(id));
		}
		if (null != model.getParentId()) {
			Org parent = orgSrv.get(model.getParentId());
			if (null != parent) {
				setAttr("parent", parent);
			}
		}
		setAttr("model", model);
		render(FORM_PAGE);
	}

	@Valid(type = Org.class)
	public boolean saveOrUpdate(Org model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		boolean success = false;
		if (null == model.getId()) {
			if (null != orgSrv.getByCode(model.getCode())) {
				ResponseModel<String> res = new ResponseModel<String>(false);
				res.setMsg("机构编号[" + model.getCode() + "]已存在!");
				renderJson(res);
				return false;
			}
			success = orgSrv.save(model);
		} else {
			success = orgSrv.update(model);
		}
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setData(model.getId().toString());
		res.setMsg("保存" + orgSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(orgSrv);
		return res.isSuccess();
	}
}