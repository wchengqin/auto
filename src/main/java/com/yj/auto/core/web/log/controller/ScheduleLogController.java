package com.yj.auto.core.web.log.controller;

import com.jfinal.log.Log;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.core.web.log.model.ScheduleLog;
import com.yj.auto.core.web.log.service.ScheduleLogService;
import com.yj.auto.helper.AutoHelper;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.utils.DateUtil;

/**
 * 任务执行日志 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "log", key = "/log/schedule")
public class ScheduleLogController extends BaseController {
	private static final Log logger = Log.getLog(ScheduleLogController.class);

	private static final String RESOURCE_URI = "log/schedule/index";
	private static final String INDEX_PAGE = "schedule_index.html";
	private static final String SHOW_PAGE = "schedule_show.html";

	ScheduleLogService scheduleLogSrv = null;

	public void index() {
		QueryModel query = new QueryModel();
		query.setStime(DateUtil.toDateStr(DateUtil.addDay(DateUtil.date(), -7)));
		setAttr("query", query);
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<ScheduleLog> dt = getDataTable(query, scheduleLogSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		ScheduleLog model = scheduleLogSrv.get(id);
		model.setSchedule(AutoHelper.getScheduleService().get(model.getScheduleId()));
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(scheduleLogSrv);
		return res.isSuccess();
	}

	// 根据条件清空日志
	public boolean clear() {
		String keyword = getPara("keyword");
		String stime = getPara("stime");
		String etime = getPara("etime");
		boolean success = scheduleLogSrv.delete(keyword, stime, etime);
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setMsg("清空" + scheduleLogSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}
}