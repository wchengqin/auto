package com.yj.auto.core.web.log.service;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yj.auto.core.base.annotation.Service;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.web.log.model.ReadingLog;

/**
 * Reading 管理 描述：
 */
@Service(name = "readingLogSrv")
public class ReadingLogService extends BaseService<ReadingLog> {

	private static final Log logger = Log.getLog(ReadingLogService.class);

	public static final String SQL_LIST = "log.reading.list";

	public static final ReadingLog dao = new ReadingLog().dao();

	@Override
	public ReadingLog getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("read_time desc");
		}
		return getSqlPara(SQL_LIST, query);
	}

	public boolean delete(String type, String keyword, String stime, String etime) {
		QueryModel query = new QueryModel();
		query.setType(type);
		query.setKeyword(keyword);
		query.setStime(stime);
		query.setEtime(etime);
		SqlPara sql = getSqlPara("log.reading.clear", query);
		return Db.update(sql) > 0;
	}

	public boolean delete(String type, Integer... ids) {
		QueryModel query = new QueryModel();
		query.setType(type);
		query.put("idArray", ids);
		SqlPara sql = getSqlPara("log.reading.delete", query);
		return Db.update(sql) > 0;
	}

}