package com.yj.auto.core.web.system.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.system.model.bean.*;
/**
 * 系统用户角色组
 */
@SuppressWarnings("serial")
@Table(name = UserRole.TABLE_NAME, key = UserRole.TABLE_PK, remark = UserRole.TABLE_REMARK)
public class UserRole extends UserRoleEntity<UserRole> {

}