package com.yj.auto.core.jfinal.plugin;

import com.jfinal.plugin.IPlugin;
import com.yj.auto.utils.QuartzUtil;

public class QuartzPlugin implements IPlugin {

	public QuartzPlugin() {
	}

	@Override
	public boolean start() {
		QuartzUtil.me().startJobs();
		return true;
	}

	@Override
	public boolean stop() {
		QuartzUtil.me().shutdownJobs();
		return true;
	}

}
