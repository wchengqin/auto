package com.yj.auto.helper;

import java.util.ArrayList;
import java.util.List;

import com.yj.auto.Constants;
import com.yj.auto.core.web.system.model.Dict;
import com.yj.auto.core.web.system.service.DictService;

public class CacheHelper {
	// 根据字典类型，得到字典列表
	public static List<Dict> getDicts(String code) {
		List<Dict> list = new ArrayList<Dict>();
		DictService srv = AutoHelper.getService("dictSrv");
		List<Dict> all = srv.getCache(code);
		if (null != all) {
			for (Dict d : all) {// 禁用字典过滤
				if (!Constants.IS_VALID_DATA(d.getState()))
					continue;
				list.add(d);
			}
		}
		return list;
	}

	// 根据字典类型，字典值，得到字典对象
	public static Dict getDict(String code, String val) {
		DictService srv = AutoHelper.getService("dictSrv");
		List<Dict> all = srv.getCache(code);
		if (null != all) {
			for (Dict d : all) {
				if (d.getVal().equals(val))
					return d;
			}
		}
		return null;
	}
}
