package com.yj.auto.component.base.model;

import com.yj.auto.component.base.model.bean.DocTypeEntity;
import com.yj.auto.core.base.annotation.Table;

/**
 * 知识库类型
 */
@SuppressWarnings("serial")
@Table(name = DocType.TABLE_NAME, key = DocType.TABLE_PK, remark = DocType.TABLE_REMARK)
public class DocType extends DocTypeEntity<DocType> {
	private DocType parent;

	public DocType getParent() {
		return parent;
	}

	public void setParent(DocType parent) {
		this.parent = parent;
	}

}