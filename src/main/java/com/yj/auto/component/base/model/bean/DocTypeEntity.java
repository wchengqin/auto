package com.yj.auto.component.base.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import com.yj.auto.core.jfinal.base.*;

/**
 *  知识库类型
 */
@SuppressWarnings("serial")
public abstract class DocTypeEntity<M extends DocTypeEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_com_doc_type"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "知识库类型"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 类型主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：name
	 * @return 类型名称
	 */
   	@NotBlank
	@Length(max = 64)	
	public String getName(){
   		return get("name");
   	}
	
	public void setName(String name){
   		set("name" , name);
   	}	
   		
	/**
	 * Column ：parent_id
	 * @return 上级类型
	 */
   	@NotNull 	
	public Integer getParentId(){
   		return get("parent_id");
   	}
	
	public void setParentId(Integer parentId){
   		set("parent_id" , parentId);
   	}	
   		
	/**
	 * Column ：state
	 * @return 类型状态
	 */
   	@NotBlank
	@Length(max = 32)	
	public String getState(){
   		return get("state");
   	}
	
	public void setState(String state){
   		set("state" , state);
   	}	
   		
	/**
	 * Column ：path
	 * @return 类型路径
	 */
   	@Length(max = 256)	
	public String getPath(){
   		return get("path");
   	}
	
	public void setPath(String path){
   		set("path" , path);
   	}	
   		
	/**
	 * Column ：remark
	 * @return 类型描述
	 */
   	@Length(max = 1024)	
	public String getRemark(){
   		return get("remark");
   	}
	
	public void setRemark(String remark){
   		set("remark" , remark);
   	}	
   		
	/**
	 * Column ：sort
	 * @return 排序号
	 */
   	@NotNull 	
	public Integer getSort(){
   		return get("sort");
   	}
	
	public void setSort(Integer sort){
   		set("sort" , sort);
   	}	
   		
	/**
	 * Column ：param1
	 * @return 参数1
	 */
   	@Length(max = 512)	
	public String getParam1(){
   		return get("param1");
   	}
	
	public void setParam1(String param1){
   		set("param1" , param1);
   	}	
   		
	/**
	 * Column ：param2
	 * @return 参数2
	 */
   	@Length(max = 512)	
	public String getParam2(){
   		return get("param2");
   	}
	
	public void setParam2(String param2){
   		set("param2" , param2);
   	}	
   		
	/**
	 * Column ：param3
	 * @return 参数3
	 */
   	@Length(max = 512)	
	public String getParam3(){
   		return get("param3");
   	}
	
	public void setParam3(String param3){
   		set("param3" , param3);
   	}	
   		
	/**
	 * Column ：luser
	 * @return 最后修改人
	 */
   		
	public Integer getLuser(){
   		return get("luser");
   	}
	
	public void setLuser(Integer luser){
   		set("luser" , luser);
   	}	
   		
	/**
	 * Column ：ltime
	 * @return 最后修改时间
	 */
   		
	public Date getLtime(){
   		return get("ltime");
   	}
	
	public void setLtime(Date ltime){
   		set("ltime" , ltime);
   	}	
}