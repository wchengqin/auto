package com.yj.auto.job.impl;

import org.quartz.JobExecutionContext;

import cn.hutool.core.io.FileUtil;
import com.yj.auto.Constants;
import com.yj.auto.job.AutoJob;

// 清除无效附件
public class ClearTempFileJob extends AutoJob {

	@Override
	public void run(JobExecutionContext context) {
		FileUtil.del(Constants.SYS_TEMP_FILE_ROOT);
	}

}
