package com.yj.auto.job.impl;

import org.quartz.JobExecutionContext;

import cn.hutool.core.date.DateTime;
import com.yj.auto.helper.LogHelper;
import com.yj.auto.job.AutoJob;
import com.yj.auto.utils.DateUtil;

// 清除历史日志
public class ClearLogHistoryJob extends AutoJob {
	@Override
	public void run(JobExecutionContext context) {
		context.getJobDetail().getKey().getName();
		DateTime now = DateUtil.date(); // 删除30天前的登录日志
		LogHelper.getLoginLogService().delete(null, null, DateUtil.toDateStr(DateUtil.addDay(now, -30)));
		// 删除7天前的访问日志
		LogHelper.getAccessLogService().delete(null, null, DateUtil.toDateStr(DateUtil.addDay(now, -7)));
		// 删除7天前的任务执行日志
		LogHelper.getScheduleLogService().delete(null, null, DateUtil.toDateStr(DateUtil.addDay(now, -7)));
	}
}
