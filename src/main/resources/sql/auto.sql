#sql("base.find.array")
	select * from #(table) where #(column) in ( #for(id:array)#(for.index>0?",":"") #(id) #end )
#end
#sql("base.delete.array")
	delete from #(table) where #(column) in ( #for(id:array)#(for.index>0?",":"") #(id) #end )
#end
#namespace("system")
	#include("system.sql")
#end
#namespace("log")
	#include("log.sql")
#end
#namespace("component")
	#include("component.sql")
#end